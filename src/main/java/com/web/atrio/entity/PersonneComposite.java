package com.web.atrio.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class PersonneComposite extends Personne{

	
	private int age;
	
	@JsonIgnoreProperties({ "personne"})
	private Emploi emploiActuelle;

	public PersonneComposite() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Emploi getEmploiActuelle() {
		return emploiActuelle;
	}

	public void setEmploiActuelle(Emploi emploiActuelle) {
		this.emploiActuelle = emploiActuelle;
	}

	
	
	
}
