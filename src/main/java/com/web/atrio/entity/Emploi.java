package com.web.atrio.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonFormat;



@Entity
public class Emploi {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="personne_id", nullable=false)
	private Personne personne;
	
	private String nomEntreprise;
	
	private String posteOccupe;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateDebut;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	private LocalDate dateFin;

	public Emploi() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Emploi(Personne personne, String nomEntreprise, String posteOccupe, LocalDate dateDebut, LocalDate dateFin) {
		super();
		this.personne = personne;
		this.nomEntreprise = nomEntreprise;
		this.posteOccupe = posteOccupe;
		this.dateDebut = dateDebut;
		this.dateFin = dateFin;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Personne getPersonne() {
		return personne;
	}

	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	public String getNomEntreprise() {
		return nomEntreprise;
	}

	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise = nomEntreprise;
	}

	public String getPosteOccupe() {
		return posteOccupe;
	}

	public void setPosteOccupe(String posteOccupe) {
		this.posteOccupe = posteOccupe;
	}

	public LocalDate getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(LocalDate dateDebut) {
		this.dateDebut = dateDebut;
	}

	public LocalDate getDateFin() {
		return dateFin;
	}

	public void setDateFin(LocalDate dateFin) {
		this.dateFin = dateFin;
	}

	@Override
	public String toString() {
		return "[ nom de l'entreprise = " + nomEntreprise + ", poste occupe = " + posteOccupe + ", date Debut = " + dateDebut
				+ ", date Fin = " + dateFin + "]";
	}
	
	

}
