package com.web.atrio.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.web.atrio.entity.Emploi;
import com.web.atrio.entity.Personne;
import com.web.atrio.entity.PersonneComposite;
import com.web.atrio.repository.EmploiRepository;
import com.web.atrio.repository.PersonneRepository;

@Service
public class PersonneService {

	@Autowired
	private PersonneRepository personneRepository;

	@Autowired
	private EmploiRepository emploiRepository;

	public Personne createPersonne(Personne personne) {

		personneRepository.save(personne);
		if (personne.getEmplois() != null) {
			Iterator<Emploi> listEmploi = personne.getEmplois().iterator();
			while (listEmploi.hasNext()) {
				Emploi emploi = listEmploi.next();
				emploi.setPersonne(personne);
				emploiRepository.save(emploi);
			}
		}
		return personne;
	}

	public List<PersonneComposite> getAllSortedEmployeesByAlphabeticalOrder() {
		List<PersonneComposite> listPersonneWithEmploi = new ArrayList<>();
		List<Personne> personnesAvecEmplois = personneRepository.findAll();
		Collections.sort(personnesAvecEmplois, (e1, e2) -> e1.getNom().compareTo(e2.getNom()));
		Iterator<Personne> it = personnesAvecEmplois.iterator();
		while (it.hasNext()) {
			Personne personne = it.next();
			PersonneComposite cp = new PersonneComposite();
			cp.setNom(personne.getNom());
			cp.setPrenom(personne.getPrenom());
			cp.setDateDeNaissance(personne.getDateDeNaissance());
			cp.setAge(calculateAge(personne.getDateDeNaissance()));
			Emploi foundElement = getActuellEmploi(personne);
			cp.setEmploiActuelle(foundElement);
			cp.setEmplois(personne.getEmplois());
			cp.setId(personne.getId());
			listPersonneWithEmploi.add(cp);
		}

		return listPersonneWithEmploi;
	}

	private Emploi getActuellEmploi(Personne personne) {
		return  personne.getEmplois().stream().filter(element -> element.getDateFin() == null)
				.findFirst().orElse(null);
	}

	public List<Personne> getPersonneByCompanyName(String companyName) {
		List<Personne> listPersonnes = new ArrayList<>();
		List<Emploi> listEmplois = emploiRepository.findByNomEntreprise(companyName);

		Iterator<Emploi> it = listEmplois.iterator();
		while (it.hasNext()) {
			Emploi emploi = it.next();
			listPersonnes.add(emploi.getPersonne());

		}
		return listPersonnes;

	}

	public Personne getEmploisByPersonneBetweenDateRange(Long idPersonne, LocalDate dateDebut, LocalDate dateFin) {

		Personne persone = null;
		Personne personneCible = null;
		Set<Emploi> newSetEmploi = new HashSet<Emploi>();
		List<Emploi> listEmplois = emploiRepository.findAll();
		Iterator<Emploi> it = listEmplois.iterator();
		while (it.hasNext()) {
			Emploi emploi = it.next();
			persone = emploi.getPersonne();
			if (emploi.getPersonne().getId().equals(idPersonne)) {
				LocalDate emploiStartDate = emploi.getDateDebut();
				LocalDate emploiEndDate = emploi.getDateFin();			
				if (emploiStartDate.isAfter(dateDebut) && emploiStartDate.isBefore(dateFin) && (emploiEndDate != null && emploiStartDate.isAfter(dateDebut)
						&& emploiEndDate.isBefore(dateFin)) || (emploiEndDate == null && emploiStartDate.isAfter(dateDebut) && LocalDate.now().isBefore(dateFin))) {
					newSetEmploi.add(emploi);
	            }
				personneCible = persone;
			}

		}
		if (!newSetEmploi.isEmpty() && personneCible != null) {
			personneCible.setEmplois(newSetEmploi);
		} else if (personneCible != null) {
			personneCible.setEmplois(null);
		}
		return personneCible;

	}

	public List<Personne> getAllEmployees() {
		return personneRepository.findAll();
	}

	private int calculateAge(LocalDate dateDeNaissance) {
		return LocalDate.now().getYear() - dateDeNaissance.getYear();
	}
}
