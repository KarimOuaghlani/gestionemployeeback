package com.web.atrio.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.web.atrio.entity.Emploi;
import com.web.atrio.entity.Personne;
import com.web.atrio.repository.EmploiRepository;
import com.web.atrio.repository.PersonneRepository;

@Service
public class EmploiService {

	@Autowired
	private PersonneRepository personneRepository;

	@Autowired
	private EmploiRepository emploiRepository;

	public ResponseEntity<String> createEmploi(List<Emploi> listEmploi, long idPersonne) {
		ResponseEntity<String> response = null;
		Personne personne = null;
		if (personneRepository.findById(idPersonne).isPresent()) {
			personne = personneRepository.findById(idPersonne).get();
		} else {
			return ResponseEntity.badRequest().body("La personne est introuvable ou inexistante");
		}
		if (!listEmploi.isEmpty() && listEmploi.size() > 1) {
			for (Emploi emploi : listEmploi) {
				response = saveJob(listEmploi, personne, emploi);
			}
		} else {
			Emploi emploi = listEmploi.get(0);
			response = saveJob(listEmploi, personne, emploi);
		}
		return response;

	}

	private ResponseEntity<String> saveJob(List<Emploi> listEmploi, Personne personne, Emploi emploi) {
		emploi.setPersonne(personne);
		try {
			if (emploi.getDateFin() != null && emploi.getDateDebut().isAfter(emploi.getDateFin())) {
				return ResponseEntity.badRequest().body("La date de début doit précéder la date de fin.");
			}
			boolean overlap = jobsOverlap(emploi, listEmploi);
			if (overlap) {
				return ResponseEntity.badRequest()
						.body("L'emploi :" + emploi + " se chevauche avec un emploi existant.");
			}
			emploiRepository.save(emploi);
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("Échec de l'ajout de l'emploi : " + emploi);
		}
		return ResponseEntity.ok("Emploi ajouté avec succès.");
	}

	public boolean jobsOverlap(Emploi nouvelEmploi, List<Emploi> newListEmploisExistant) {
		List<Emploi> listEmploisExistant = new ArrayList<>(newListEmploisExistant);
		listEmploisExistant.remove(nouvelEmploi);
		List<Emploi> oldList = emploiRepository.findByPersonneId(nouvelEmploi.getPersonne().getId());
		listEmploisExistant.addAll(oldList);
		for (Emploi emploiExistant : listEmploisExistant) {
			if (nouvelEmploi.getDateFin() == null) {
				if (!nouvelEmploi.getDateDebut().isAfter(emploiExistant.getDateFin())) {
					return true;
				}
			} else {
				if (emploiExistant.getDateFin() != null && !nouvelEmploi.getDateDebut().isAfter(emploiExistant.getDateFin())
						&& !nouvelEmploi.getDateFin().isBefore(emploiExistant.getDateDebut())) {
					return true;
				}
			}
		}
		return false;
	}
}
