package com.web.atrio.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.web.atrio.entity.Personne;

@Repository
public interface PersonneRepository extends JpaRepository<Personne, Long> {
}
