package com.web.atrio.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.web.atrio.entity.Emploi;

@Repository
public interface EmploiRepository extends JpaRepository<Emploi, Long> {
	List<Emploi> findByPersonneId(Long personneId);
	
	List<Emploi> findByNomEntreprise(String nomEntreprise);
	
	@Query(value = "SELECT * FROM EMPLOI u WHERE u.date_debut >= ?1 or u.date_fin <= ?2", 
		    nativeQuery = true)
    List<Emploi> findByDateDebutBetween(LocalDate startDate, LocalDate endDate);


}
