package com.web.atrio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.web.atrio.entity.Emploi;
import com.web.atrio.service.EmploiService;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/emplois")
public class EmploiController {

	@Autowired
	private EmploiService emploiService;


	@PostMapping(value = "/{idPersonne}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createEmploi(@PathVariable(value = "idPersonne")  long idPersonne, @RequestBody List<Emploi> listEmploi) {
		return new ResponseEntity<>(emploiService.createEmploi(listEmploi, idPersonne), HttpStatus.OK);
	}

}
