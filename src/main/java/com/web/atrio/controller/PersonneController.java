package com.web.atrio.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.web.atrio.entity.Personne;
import com.web.atrio.entity.PersonneComposite;
import com.web.atrio.service.PersonneService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/personnes")
public class PersonneController {

	@Autowired
	private PersonneService personneService;

	@PostMapping
	//@PreAuthorize
	public ResponseEntity<?> createPersonne(@RequestBody Personne personne) {
		if (calculateAge(personne.getDateDeNaissance()) < 150) {
			Personne newPersonne = personneService.createPersonne(personne);
			return ResponseEntity.ok(newPersonne);
		} else {
			return ResponseEntity.badRequest().body("L'âge doit être inférieur à 150 ans.");
		}
	}

	private int calculateAge(LocalDate dateDeNaissance) {
		return LocalDate.now().getYear() - dateDeNaissance.getYear();
	}

	@GetMapping(value = "/sorted", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PersonneComposite>> getAllSortedEmployeesByAlphabeticalOrder() {
		return new ResponseEntity<>(personneService.getAllSortedEmployeesByAlphabeticalOrder(), HttpStatus.OK);
	}

	@GetMapping(value = "/{companyName}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Personne>> getPersonneByCompanyName(
			@PathVariable(value = "companyName") String companyName) {
		return new ResponseEntity<>(personneService.getPersonneByCompanyName(companyName), HttpStatus.OK);
	}

	@GetMapping(value = "/between", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Personne> getEmploisByPersonneBetweenDateRange(@RequestParam(value = "idPersonne", required = false) Long idPersonne,
			@RequestParam(value = "dateDebut", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateDebut,
			@RequestParam(value = "dateFin", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate dateFin) {
		return new ResponseEntity<>(personneService.getEmploisByPersonneBetweenDateRange(idPersonne, dateDebut, dateFin), HttpStatus.OK);
	}
	
	
	@GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Personne>> getAllEmployees() {
		return new ResponseEntity<>(personneService.getAllEmployees(), HttpStatus.OK);
	}

}
